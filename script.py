import sqlite3
from flask import Flask, render_template, jsonify, request,redirect
from flask_cors import CORS
import json
import os
app = Flask(__name__)
CORS(app)
server_reponse={
	'ack':"",
	'data':""
}

# functio to create a table in the local pc using sqlite if the tables are not present.
def create_table():
	try:
		
		# Connect to DB and create a cursor
		sqliteConnection = sqlite3.connect('sql.db')
		cursor = sqliteConnection.cursor()
		print('DB Init')

		# Write a query and execute it with cursor
		query = 'select sqlite_version();'
		cursor.execute(query)

		# Fetch and output result
		result = cursor.fetchall()
		print('SQLite Version is {}'.format(result))

		# cursor object
		#cursor_obj = connection_obj.cursor()
		
		# Drop the GEEK table if already exists.
		#cursor.execute("DROP TABLE IF EXISTS DOWNLOADS")
		
		# Creating table
		table = """ CREATE TABLE IF NOT EXISTS DOWNLOADS (
					Title VARCHAR(255) NOT NULL,
					URL VARCHAR(255) NOT NULL,
					Language CHAR(25),
					Domain CHAR(25),
					PageID CHAR(25),
					Local_path VARCHAR(255)
				); """
		
		cursor.execute(table)
		
		print("Table is Ready")

		# Close the cursor
		cursor.close()

	# Handle errors
	except sqlite3.Error as error:
		print('Error occured - ', error)

	# Close DB Connection irrespective of success
	# or failure
	finally:
		
		if sqliteConnection:
			sqliteConnection.close()
			print('SQLite Connection closed')

# writing the downloaded data into the json file and storing in the downloads folder.
def write_into_json_file(data_to_write):
	# Serializing json
	json_object = json.dumps(data_to_write, indent=4)

	# Writing to sample.json
	with open("downloads/"+data_to_write['title']+".json", "w") as outfile:
		outfile.write(json_object)

@app.route('/test')
# ‘/’ URL is bound with hello_world() function.
def hello_world():
    return 'Hello World'

# Inserting data into database.
@app.route('/db/write',methods=['POST'])
def insert_data():
	data = request.data    
	#print(">>",data)  
	y = json.loads(data)
	print(y)

	conn = sqlite3.connect('sql.db')
	
	# Creating a cursor object using the 
	# cursor() method
	cursor = conn.cursor()
	basepath = './downloads/'
	try:

		cursor.execute("SELECT * FROM DOWNLOADS WHERE URL = ?", (y['url'],))
		data=cursor.fetchall()
		if len(data)==0:
			print('There is no article titiled as: ',y['title'])
			basepath = './downloads/'+y['title']+'.json'
			# Queries to INSERT records.
			cursor.execute("INSERT INTO DOWNLOADS VALUES ('"+y['title']+"', '"+y['url']+"', '"+y['lang']+"','"+y['domain']+"',"+str(y['pageID'])+",'"+basepath+"')")
			# Display data inserted
			print("Data Inserted in the table: ")
			write_into_json_file(y)
			server_reponse['ack']="Data Inserted in the table successfuly"
			data=cursor.execute('''SELECT * FROM DOWNLOADS''')
			for row in data:
				print(row)
			# Commit your changes in the database    
			conn.commit()
			
			# Closing the connection
			conn.close()
		else:
			print('Article already exists with title : ',y['title'])
			server_reponse['ack']="Article already exists with title : "+y['title']
	# Handle errors
	except sqlite3.Error as error:
		print('Error occured - ', error)
		server_reponse['ack']="Error occured"
	
	return jsonify(server_reponse)

# reading data from database
@app.route('/db/read',methods=['POST'])
def read_data():
	data = request.data    
	print(">>",data)  
	y = json.loads(data)
	print(y)
	conn = sqlite3.connect('sql.db')
	response=""
	# Creating a cursor object using the 
	# cursor() method
	cursor = conn.cursor()
	try:
		data=cursor.execute('''SELECT * FROM DOWNLOADS''')
		final_data=[]
		print("final:",final_data)
		for row in data:
			print("response:",row)
			final_data.append(row)
			json.dumps(final_data)
		conn.close()
	except sqlite3.Error as error:
		print('Error occured - ', error)
	
	return jsonify(final_data)		
		
	#json.dumps(final_data)

# get the list of files present in the downloads forlder.
@app.route('/listfiles', methods=['POST'])
def listfiles():
	basepath = './downloads'
	flist=""
	first=0
	for entry in os.scandir(basepath):
		if entry.is_dir():    # skip directories
			continue
		else:
			if first==0:
				first=1
				flist=entry.name
			else:
				flist=flist+","+entry.name
	print("files=", flist) # use entry.path to get the full path of this entry, or use entry.name for the base filename
	if (len(flist)>0):
		server_reponse['ack']="List of files found"; server_reponse['data']=flist
	else:
		server_reponse['ack']="no files found"
	return jsonify(server_reponse)

# get the data of the selected file.
@app.route('/getfile', methods=['POST'])
def getfile():
	data = request.data    
	print(">>",data)  
	y = json.loads(data)
	print(y)
	print(y['file'])
	import os.path
	file_path='./downloads/'+y['file']
	file_exists = os.path.exists(file_path)
	if(file_exists==True):
		f = open(file_path)
		file_data = json.load(f)
		server_reponse['ack']="file found successfully"
		server_reponse['data']=file_data
	else:
		server_reponse['ack']="file not found"
	
	return jsonify(server_reponse)

create_table()		
if __name__ == '__main__':
	app.run()
